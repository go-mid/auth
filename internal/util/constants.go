package util

type EnumStatus int

const (
	//正常
	EnumStatus_NORMAL EnumStatus = 1
	//已删除
	EnumStatus_DEL EnumStatus = 100
)
