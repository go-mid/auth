package dao

import (
	"context"
	"gitee.com/go-mid/auth/internal/entity"
	"gitee.com/go-mid/infra/xsql/xdb"
)

func GetAuthTokenByToken(ctx context.Context, db xdb.XDB, token string) (*entity.AuthToken, error) {
	var item entity.AuthToken
	err := db.SelectOne(ctx, entity.AuthTokenTable, map[string]interface{}{
		"access_token": token,
	}, &item)

	if xdb.IsNotFound(err) {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	return &item, nil
}
