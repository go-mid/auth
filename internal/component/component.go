package component

import (
	"context"
	"gitee.com/go-mid/booter/bootservice"
	"gitee.com/go-mid/booter/server"
	"gitee.com/go-mid/infra/xsql/xdb"
)

var XDBAuth *xdb.DB

var ServicesMap = make(map[string]bootservice.Service)
var Sbase server.Server

func InitComponent(ctx context.Context, sb server.Server) error {
	Sbase = sb
	err := LoadConfig(sb)
	if err != nil {
		return err
	}
	if err := initDbManager(ctx, sb); err != nil {
		return err
	}
	return nil
}
func initDbManager(ctx context.Context, sb server.Server) error {
	if xsqlMgr, err := xdb.NewManager(sb.ConfigCenter(ctx), ""); err != nil {
		return err
	} else {
		if db, err := xsqlMgr.GetDB(ctx, "auth"); err != nil {
			return err
		} else {
			XDBAuth = db
		}
	}
	return nil
}
func LoadConfig(sb server.Server) error {
	return nil
}
func RegisterService(ctx context.Context, name string, service bootservice.Service) error {

	return nil
}

func InitService(ctx context.Context, sb server.Server) error {

	return nil
}
