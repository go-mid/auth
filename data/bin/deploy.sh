#!/bin/bash
cd `dirname $0`
BIN_DIR=`pwd`
ENV="dev"
if [ -n "$1" ]; then
	ENV="$1"
fi
cd ..
DEPLOY_DIR=`pwd`
CURRENT=$(date +%Y%m%d%H%M%S)
## 修改此处配置为本机的信息 start===
SERVER_NAME="auth"
GIT_REPO_NAME="auth"
SERVER_PORT="8888"
if (( $ENV=="pro" )); then
	SERVER_PORT="9999"
fi
GOROOT="/usr/admin/go1.8"
## 修改此处配置为本机的信息 end=====

echo -e "Pull the $SERVER_NAME ...\c"
cd /data/$ENV/src
git config --global url."git@gitee.com:go-mid/".insteadOf "https://gitee.com/go-mid/"
git clone git@gitee.com:go-mid/$GIT_REPO_NAME.git
export GOROOT=$GOROOT
export PATH=$GOROOT/bin/:$PATH
export GOPROXY=https://goproxy.cn
export GOPRIVATE=gitee.com/go-mid/
export SNAME=$SERVER_NAME
export SPORT=$SERVER_PORT
export SENV=$ENV
cd ./$GIT_REPO_NAME
$GOROOT/bin/go build
if [ -e "/data/$ENV/pkg/$GIT_REPO_NAME" ]; then
  mv /data/$ENV/pkg/$GIT_REPO_NAME /data/$ENV/pkg/$GIT_REPO_NAME-$CURRENT
fi
mv $GIT_REPO_NAME /data/$ENV/pkg/$GIT_REPO_NAME
rm -rf /data/$ENV/src/$GIT_REPO_NAME
cd /data
./bin/restart.sh