#!/bin/bash
cd `dirname $0`
BIN_DIR=`pwd`
cd ..
DEPLOY_DIR=`pwd`
## 修改此处配置为本机的信息 start===
ENV=$1
SERVER_GROUP=$2
SERVER_NAME=$3
SERVER_PORT=$4
CONFROOT="/data/$ENV/conf"
## 修改此处配置为本机的信息 end=====

if [ -z "$SERVER_NAME" ]; then
SERVER_NAME=`hostname`
fi


PIDS=`ps -ef |grep "$SERVER_NAME" |grep "$ENV" | grep -v "grep" | grep -v ".sh" | awk '{print $2}'`
if [ -n "$PIDS" ]; then
    echo "ERROR: The $SERVER_NAME already started!"
    echo "PID: $PIDS"
    exit 1
fi

if [ -n "$SERVER_PORT" ]; then
    SERVER_PORT_COUNT=`netstat -tln | grep $SERVER_PORT | wc -l`
    if [ $SERVER_PORT_COUNT -gt 0 ]; then
        echo "ERROR: The $SERVER_NAME port $SERVER_PORT already used!"
        exit 1
    fi
fi
STDOUT_FILE=$DEPLOY_DIR/$ENV/stdout.log
echo -e "Starting the $SERVER_NAME ...\c"

nohup  /data/$ENV/pkg/$SERVER_NAME -cfgroot $CONFROOT  > $STDOUT_FILE 2>&1 &

COUNT=0
while [ $COUNT -lt 1 ]; do
    echo -e ".\c"
    sleep 1
    if [ -n "$SERVER_PORT" ]; then
        COUNT=`netstat -an | grep $SERVER_PORT | wc -l`
    fi
    if [ $COUNT -lt 1 ]; then
        COUNT=`ps -f | grep "$SERVER_NAME" | grep -v "grep" | awk '{print $2}' | wc -l`
    fi
    if [ $COUNT -gt 0 ]; then
        break
    fi
done

echo "OK!"
PIDS=`ps -f | grep "$SERVER_NAME" | grep -v "grep" | awk '{print $2}'`
echo "PID: $PIDS"