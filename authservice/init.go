package authservice

import (
	"context"
	"gitee.com/go-mid/auth/internal/component"
	"gitee.com/go-mid/booter"
	"gitee.com/go-mid/booter/server"
	"gitee.com/go-mid/infra/xlog"
	"math/rand"
	"time"
)

func init() {
	booter.AppendPreServiceDriverFuncs(InitLogic)
}
func InitLogic(ctx context.Context, sb server.Server) error {
	rand.Seed(time.Now().UnixNano())
	xlog.Info(ctx, "Init Logic ", sb.Copyname(ctx))
	if err := component.InitComponent(ctx, sb); err != nil {
		return err
	}
	DefaultAuthService.initTask(ctx)
	return nil
}
