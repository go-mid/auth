package authservice

import (
	"context"
	"gitee.com/go-mid/infra/xcore"
	"github.com/kr/pretty"
	"reflect"
	"testing"
)

func TestAuthServiceImpl_GetAuthToken(t *testing.T) {
	type args struct {
		ctx context.Context
		req *GetAuthTokenReq
	}
	tests := []struct {
		name    string
		args    args
		want    *GetAuthTokenRes
		wantErr bool
	}{
		{
			name: "test1",
			args: args{
				ctx: context.Background(),
				req: &GetAuthTokenReq{
					ID: 1,
				},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DefaultAuthService.GetAuthToken(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAuthToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAuthToken() got = %v, want %v", xcore.JsonString(ctx, got), tt.want)
			}
		})
	}
}

func TestAuthServiceImpl_AddAuthToken(t *testing.T) {
	type args struct {
		ctx context.Context
		req *AddAuthTokenReq
	}
	tests := []struct {
		name    string
		args    args
		want    *AddAuthTokenRes
		wantErr bool
	}{
		{
			name: "test1",
			args: args{
				ctx: context.Background(),
				req: &AddAuthTokenReq{
					// todo param
				},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DefaultAuthService.AddAuthToken(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("AddAuthToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AddAuthToken() got = %v, want %v", xcore.JsonString(ctx, got), tt.want)
			}
		})
	}
}

func TestAuthServiceImpl_UpdateAuthToken(t *testing.T) {
	type args struct {
		ctx context.Context
		req *UpdateAuthTokenReq
	}
	tests := []struct {
		name    string
		args    args
		want    *UpdateAuthTokenRes
		wantErr bool
	}{
		{
			name: "test1",
			args: args{
				ctx: context.Background(),
				req: &UpdateAuthTokenReq{
					// todo param
				},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DefaultAuthService.UpdateAuthToken(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateAuthToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateAuthToken() got = %v, want %v", xcore.JsonString(ctx, got), tt.want)
			}
		})
	}
}
func TestAuthServiceImpl_UpsertAuthToken(t *testing.T) {
	type args struct {
		ctx context.Context
		req *UpsertAuthTokenReq
	}
	tests := []struct {
		name    string
		args    args
		want    *UpsertAuthTokenRes
		wantErr bool
	}{
		{
			name: "test1",
			args: args{
				ctx: context.Background(),
				req: &UpsertAuthTokenReq{
					// todo param
				},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DefaultAuthService.UpsertAuthToken(tt.args.ctx, tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpsertAuthToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpsertAuthToken() got = %v, want %v", xcore.JsonString(ctx, got), tt.want)
			}
		})
	}
}

func TestAuthServiceImpl_ListAuthToken(t *testing.T) {
	type args struct {
		ctx context.Context
		req *ListAuthTokenReq
	}
	tests := []struct {
		name    string
		args    args
		want    *ListAuthTokenRes
		wantErr bool
	}{
		{
			name: "test1",
			args: args{
				ctx: context.Background(),
				req: &ListAuthTokenReq{
					// todo param
				},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DefaultAuthService.ListAuthToken(tt.args.ctx, tt.args.req)
			pretty.Println(got)
			if (err != nil) != tt.wantErr {
				t.Errorf("ListAuthToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListAuthToken() got = %v, want %v", xcore.JsonString(ctx, got), tt.want)
			}
		})
	}
}
