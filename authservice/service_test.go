package authservice

import (
	"context"
	"flag"
	"fmt"
	"gitee.com/go-mid/booter"
	"gitee.com/go-mid/booter/server"
	"gitee.com/go-mid/infra/xconfig/xviper"
	"gitee.com/go-mid/infra/xcore"
	"gitee.com/go-mid/infra/xlog"
	"testing"
)

var ctx = context.Background()

func init() {
	testing.Init()
	flag.Parse()
	//取当前文件目录
	dir, _ := xcore.CurrentPath(1)
	// 如果初始化过程发生错误，会直接panic
	// 正常情况这个调用会直接阻塞
	//bootconf 此处设置非0值， 优先级高于命令行
	if err := booter.InitTest(&server.BootConfig{
		Sname:   "auth",
		CfgSrc:  xviper.FILE,
		Cfgtype: xviper.YAML,
		Cfgroot: fmt.Sprintf("%s/../data/dev/conf", dir),
	}); err != nil {
		xlog.Errorf(context.Background(), "serve err:%s", err)
	}
}
