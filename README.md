# service
项目采用包依赖的管理方式，依赖关系如下： 
main -> processor -> api -> controller -> service -> dao
其中
util： 为本项目的公共函数，常量等，不依赖别人，只能被依赖
rpc： 主要依赖第三方的接口函数
model: 本项目使用到的组件等的全局对象，如：全局锁、Redis客户端、MQ客户端等